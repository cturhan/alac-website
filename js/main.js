/*Global Variables*/
var oldEq = 0;
var menuOpened = false;
var nextProjectforMobile = true;
var activeDesktopProjectEQ = 0;
var totalProjects = $("ol>li:first-child").size();
var bug = true;
$(function(){


if($("html, body").width() >= 768){ // ********************************************* Tablet and PC common js
	//**********************scroll variables for projects page
	window.nextProjectforMobile = false;
	$("ul.projects>li").click(function(){				//Project header clicks event
		var projectIndex = $(this).index();
		scrollProjects(projectIndex);
	});
	$("ol>li:first-child").eq(0).addClass("active");
	$(".projectSource").scroll('scrollTop',function(){ /*ProjectSource scroll event*/
		var scrollVal 	= $(".projectSource").scrollTop();
		var cStart	  	= scrllValCalc(window.activeDesktopProjectEQ)-200;
		var cLimit		= scrllValCalc(window.activeDesktopProjectEQ+1)-200;
			if(scrollVal>cStart && scrollVal<cLimit){
				$("ol>li").removeClass("active"); $("ol>li:first-child").eq(window.activeDesktopProjectEQ).addClass("active"); 
			}else if(scrollVal<cLimit){
				if(window.bug===false){
				$("ol>li").removeClass("active"); $("ol>li:first-child").eq(--window.activeDesktopProjectEQ).addClass("active");
				window.bug=true
				}else {
				window.bug=false;
				}
			}
			else{
			$("ol>li").removeClass("active"); $("ol>li:first-child").eq(++window.activeDesktopProjectEQ).addClass("active");
			}
	});/*scroll event function end*/
	
}else{/*********************************************************************************only mobile js
/*Projects Transition for Mobile*/
	$(".project:not(:first)").css('display','none');		//All project display none not:first
	$("ol>li:first-child").eq(0).addClass("active");		//First project active class
	transitionProject(0);									//First run function for undefined variables error and opened effect
	$("ul.projects>li").click(function(){					//Project header clicks event
		var projectIndex = $(this).index();
		transitionProject(projectIndex);
	});
} //elseEnd

});	// On Load Run is End
/***********************************************Global Functions*************************************/
function transitionProject(eq){
	newEq 		  = eq;
	totalProjects = $("ol>li:first-child").size();
	projectHeight = $(".project").height();
	projectWidth  = $(".project").width();
	$newEq = $(".project:eq("+eq+")");
	$oldEq = $(".project:eq("+window.oldEq+")");
	$("ol>li").removeClass("active");
	$("ol>li:first-child").eq(eq).addClass("active");
	if(eq!=window.oldEq){
		$newEq.css({										//new project z-index and new options
			'display':'block',
			"margin-left":projectWidth+"px",
			"z-index":"1",
			"margin-top":(eq>window.oldEq ? -(projectHeight) : 0)+"px",
			"box-shadow":"-5px 0 100px rgba(0,0,0,.8)"
		});
		$(".projectSource").css('background','#000');
		/*tweenMax animations*/
		TweenLite.to($newEq, .25, {"margin-left":0});
		TweenLite.to($oldEq, .25, {opacity:.5, display:"none"});
		if(eq<window.oldEq){TweenLite.to($oldEq, 0, {marginTop:-projectHeight});}; // if eq<oldEq old project margin-top - val x2
		/*Reset CSS after animation for newEq*/
		TweenLite.to($newEq, 0, {delay:.25, css:{"z-index":"0","margin":"0","box-shadow":"none"}});
		TweenLite.to($oldEq, 0, {delay:.25, opacity:1});
		/*-------------------*/
		$("html, body").stop().animate({ scrollTop: $(this).height()}, 500);
		window.oldEq=eq;
	};
};
/*"Next Project Button for Projects Page*/
function nextProject(){
	if(window.nextProjectforMobile===true){ //for mobile settings
		if(newEq < totalProjects-1){// -1 = show one project
			transitionProject(++newEq)
		}else{newEq=0; transitionProject(newEq)}
	}else{
		if(window.activeDesktopProjectEQ < totalProjects-1){
		scrollProjects(++window.activeDesktopProjectEQ)
		}else{window.activeDesktopProjectEQ=0; scrollProjects(window.activeDesktopProjectEQ)}
	}
};

function scrollDown(){
	$("html, body").stop().animate({ scrollTop: $(this).height()}, 500)	
};	
function topMenu(){
	if(window.menuOpened===false){
		window.menuOpened=true;
		$("html").css("overflow","hidden");
		TweenLite.to(".topMenu", 0, {display:"block",height:0});
		TweenLite.to(".topMenu", .3, {height:"100vh"});
		TweenLite.from(".topMenu h2, .topMenu h3", 1.4, {opacity:0});
		TweenLite.from(".topMenu h2, .topMenu h3", 0.4, {scale:1.5});
		TweenLite.to(".topMenu h2, .topMenu h3", 0, {delay:1.5,scale:1, opacity:1});
		/*menuIcon animations*/
		TweenLite.from(".menuIcon .line", .6, {scale:1.2, ease: Power1.easeOut});
		TweenLite.to(".menuIcon .line:eq(0)", .2, {opacity:0});
		TweenLite.to(".menuIcon .line:eq(1)", .3, {rotation:"45", marginTop:0, ease: Power1.easeOut});
		TweenLite.to(".menuIcon .line:eq(2)", .3, {rotation:"-45", marginTop:0, top:-4, ease: Power1.easeOut});
		TweenLite.to(".menuIcon .line", 0, {delay:1,scale:1});		
	}else{
		window.menuOpened=false;
		$("html").css("overflow","auto");
		TweenLite.to(".topMenu", .3, {height:0});
		TweenLite.to(".topMenu", 0, {delay:.6,display:"none"});
		/*menuIcon animations*/
		TweenLite.to(".menuIcon .line:eq(0)", .2, {opacity:1});
		TweenLite.to(".menuIcon .line:eq(1)", .3, {rotation:"0",marginTop:6});
		TweenLite.to(".menuIcon .line:eq(2)", .3, {rotation:"0", marginTop:6, top:0});
	}
};
function scrollProjects(projectIndex){ /*for projects page*/
	var scrllVal			= scrllValCalc(projectIndex);
	$(".projectSource").stop().animate({ scrollTop: scrllVal}, 500);
};

function scrllValCalc(indx){
	var pSourceScrollVal 	= $(".projectSource").scrollTop();
	var headerHeight		= $(".header").height();
	var nav					= $(".project:eq(" + indx + ")");
	if (nav.length) 			{var contentNav = nav.offset().top;} // if !projects.top 
	else						{var contentNav=$(".project:eq(" +(window.totalProjects-1)+ ")").height();} // else last projectsTop height value
	var calc				= (contentNav+pSourceScrollVal) - headerHeight;
	return calc;
};

function projectPage(){ /*for projects page*/
	$(".vLine").css("height","0");
	var tl = new TimelineLite();
	TweenLite.to(".vLine",1.5,{height:"100%", ease: Power1.easeOut});
	TweenLite.to("ol", 0, {opacity:0,left:-250});
	tl.add(TweenMax.staggerTo("ol li:first-child", 0, {opacity:0},0));
	tl.add(TweenMax.staggerTo("ol", .4, {left:0,opacity:1,ease:Strong.easeOut}, 0.15));
	TweenMax.staggerTo("ol li:first-child", .5, {opacity:1,delay:.4},0.15);
}