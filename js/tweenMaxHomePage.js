/*tweenMax Home Page Animations for Mobile, Tablet and Desktop Common Js*/
var onScroll=false; /*for categories animation control*/
/*alac text animate*/
TweenLite.to(".alacText", 0, {
	scaleX:.5, 
	scaleY:.5, 
})
TweenLite.to(".alacText", 4, {
	scaleX:1, 
	scaleY:1, 
	opacity:1, 
	ease: Sine.easeOut
})
/*leadersText word animate*/
TweenLite.to(".leadersText span:nth-child(1)", 4, {delay:2.5,opacity:1});
TweenLite.to(".leadersText span:nth-child(2)", 3, {delay:3.5,opacity:1});
TweenLite.to(".leadersText span:nth-child(3)", 2, {delay:4.3,opacity:1});
repeatVal=($("html, body").width() >= 768 ? 50 : 0);
downArrowRepeat(repeatVal);
if($("html, body").width() >= 768 && $("html, body").width() < 1366){//**************************** only Tablet
	TweenLite.to(".alacLine", 4, {width:610, opacity:1, ease: Sine.easeOut})
	/*down arrow animations*/
	TweenLite.to(".downArrow img:eq(1)", 1, {delay:3, opacity:1});
	TweenLite.to(".downArrow img:eq(2)", 1, {delay:4.5, opacity:1});
	TweenLite.to(".downArrow img:eq(3),.downArrow img:eq(0)", 1, {delay:5, opacity:1});
	TweenLite.to(".downArrow img:eq(4)", 1, {delay:5.5, opacity:1});
	TweenLite.to(".downArrow img:eq(5)", 1, {delay:5.9, opacity:1});
	TweenLite.to(".downArrow p", .35, {delay:5, display:"block", opacity:1});
	TweenLite.to(".downArrow p", .35, {delay:5.35, opacity:.45});
	TweenLite.to(".downArrow p", .35, {delay:5.70, opacity:1});
	TweenLite.to(".downArrow p", .35, {delay:6.05, opacity:.45});
	TweenLite.to(".downArrow p", .35, {delay:6.40, opacity:1});
	TweenLite.to(".downArrow p", .35, {delay:6.75, opacity:.45});
	TweenLite.to(".downArrow p", .35, {delay:7.10, opacity:1});
	
}else if($("html, body").width() < 768){//*******************************************************only mobile js
	TweenLite.to(".alacLine", 4, {width:300, opacity:1, ease: Sine.easeOut})
}else{/* ***************************************************************************************only desktop js*/
	TweenLite.to(".alacLine", 4, {width:715, opacity:1, ease: Sine.easeOut})
	/*down arrow animations*/
	TweenLite.to(".downArrow p", .35, {delay:5, display:"block", opacity:1});
	TweenLite.to(".downArrow p", .35, {delay:5.35, opacity:.45});
	TweenLite.to(".downArrow p", .35, {delay:5.70, opacity:1});
	TweenLite.to(".downArrow p", .35, {delay:6.05, opacity:.45});
	TweenLite.to(".downArrow p", .35, {delay:6.40, opacity:1});
	TweenLite.to(".downArrow p", .35, {delay:6.75, opacity:.45});
	TweenLite.to(".downArrow p", .35, {delay:7.10, opacity:1});
};

if($("html, body").width() >= 768){ // ********************************************* Tablet and PC common js
	/*homepage scroll events*/
	$(document).scroll('scrollTop',function(){
		var scrollVar = $(document).scrollTop()
		if(scrollVar>=150 && window.onScroll===false) {
			categoriesAnimation();
			onScroll = true;
		};
	});
};/*end if for table and pc js*/

function categoriesAnimation(){
		$("html, body").animate({ scrollTop: $(this).height()}, 500)	
		//lines animations*******************************************************
		var $c1Line = $(".categories.c1>.hLine");
		var $c2Line = $(".categories.c2>.hLine");
		var $c4Line = $(".categories.c4>.vLine");
		TweenLite.to(".alacArea", 0,{borderBottomColor:"#a2a2a2"});
		TweenLite.to($c1Line, 1, {height:"100%", ease: Power1.easeOut}); 
		TweenLite.to($c2Line, 1, {delay:.7, height:"100%", ease: Power1.easeOut}); 
		TweenLite.to($c4Line, 1, {delay:1.2, width:"100%", ease: Power1.easeOut}); 
		//imgBox opacity animations**********************************************
		//.gTitle1, .gTitle2, .gTitle3, .gTitle4
		TweenLite.to(".gTitle1", 1, {delay:1, opacity:1, ease: Power1.easeOut, zIndex:1}); 
		TweenLite.to(".gTitle2", 1, {delay:1.2, opacity:1, ease: Power1.easeOut, zIndex:1}); 
		TweenLite.to(".gTitle3", 1, {delay:1.4, opacity:1, ease: Power1.easeOut, zIndex:1}); 
		TweenLite.to(".gTitle4", 1, {delay:1.6, opacity:1, ease: Power1.easeOut, zIndex:1}); 
		/*img[alt=about], img[alt=projects], img[alt=capabilities], img[alt=contact]*/
		var $img1 = "img[alt=about]";
		var $img2 = "img[alt=projects]";
		var $img3 = "img[alt=capabilities]";
		var $img4 = "img[alt=contact]";
		TweenLite.to($img1, 1, {delay:1.6, opacity:.7, ease: Power1.easeOut}); 
		TweenLite.to($img2, 1, {delay:1.8, opacity:1, ease: Power1.easeOut}); 
		TweenLite.to($img3, 1, {delay:2, opacity:1, ease: Power1.easeOut}); 
		TweenLite.to($img4, 1, {delay:2.2, opacity:1, ease: Power1.easeOut}); 
		//grayAlpha *************************************************************
		var $gray = $(".categories .grayAlpha");
		TweenLite.to($gray, 1, {delay:2.22, backgroundColor:"rgba(0,0,0,.20)"});
	}
function downArrowRepeat(repeatVal){
	var box = document.getElementById("repeat"),
    count = 0,
    tween;
	tween = TweenMax.to(box, .2, {repeat:repeatVal, yoyo:true, onRepeat:onRepeat, repeatDelay:.9, ease:Linear.easeNone, delay:4});
		function onRepeat() {
		  count++;
		  box.innerHTML = count;
		  TweenMax.staggerFromTo(".downArrow img", 2,{opacity:.2, immediateRender:false},{opacity:1},.19);
		}  
};